/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.natruja.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 *
 * @author Acer
 */
public class IsertUser {
     public static void main( String args[] ) {
      Connection c = null;
      Statement stmt = null;
      
      try {
         Class.forName("org.sqlite.JDBC");
         c = DriverManager.getConnection("jdbc:sqlite:test.db");
         c.setAutoCommit(false);
         System.out.println("Opened database successfully");

        stmt = c.createStatement();
         String sql = "INSERT INTO USER (ID,NAME,PASSWORD) " +
                        "VALUES (3, 'Mark','Mark93');"; 
         stmt.executeUpdate(sql);

         sql = "INSERT INTO USER (ID,NAME) " +
                  "VALUES (5, 'JB','JB94');"; 
         stmt.executeUpdate(sql);

         sql = "INSERT INTO USER (ID,NAME) " +
                  "VALUES (6, 'Jackson','Jackson95' );"; 
         stmt.executeUpdate(sql);



         c.commit();
         c.close();
      } catch ( Exception e ) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         System.exit(0);
      }
      System.out.println("Records created successfully");
   }
}
